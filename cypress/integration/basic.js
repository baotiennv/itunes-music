/* eslint-disable jest/expect-expect */
import testIds from 'constants/testIds';

describe('Itunes Music', () => {
  it('should have a correct title', () => {
    cy.visit('/');

    cy.title().should('include', 'Itunes Music');
  });

  it('should have a search bar', () => {
    cy.findByTestId(testIds.SEARCH_BAR).should('be.visible');
    cy.findByTestId(testIds.SEARCH_BAR).focus();
  });

  it('should have genre filter section', () => {
    for (let i = 0; i < 13; i++) {
      const genreButtonId = testIds.GENRE_BUTTON.replace('{{index}}', i.toString());
      cy.findByTestId(genreButtonId).should('be.visible');
    }
  });

  it('should search bar work correctly', () => {
    cy.findByTestId(testIds.SEARCH_BAR).type('Wake me up');

    // waiting to fetch data
    cy.wait(4000);

    // testing the first 20 items;
    for (let i = 0; i < 20; i++) {
      const musicPosterId = testIds.MUSIC_POSTER.replace('{{index}}', i.toString());
      cy.findByTestId(musicPosterId).should('be.visible');
    }
  });

  it('should genre filter button work correctly', () => {
    const genreButtonId = testIds.GENRE_BUTTON.replace('{{index}}', '10');
    const anotherGenreButtonId = testIds.GENRE_BUTTON.replace('{{index}}', '11');

    // avoid if current button is selected
    cy.findByTestId(anotherGenreButtonId).click();
    cy.findByTestId(genreButtonId).click();

    cy.findByTestId(anotherGenreButtonId).should(
      'have.css',
      'background-color',
      'rgba(0, 0, 0, 0)',
    );
    cy.findByTestId(genreButtonId).should('have.css', 'background-color', 'rgb(47, 144, 79)');
  });

  it('should persist storage work correctly', async () => {
    await cy.reload();

    // after reload, no need to fetch any data by API, show the list of song of persist storage
    for (let i = 0; i < 20; i++) {
      const musicPosterId = testIds.MUSIC_POSTER.replace('{{index}}', i.toString());
      cy.findByTestId(musicPosterId).should('be.visible');
    }
  });
});
