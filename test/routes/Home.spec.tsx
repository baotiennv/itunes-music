import React from 'react';

import Home from 'routes/Home';

import { render, screen } from 'test-utils';

describe('Home', () => {
  it('should render properly', () => {
    render(<Home />);
    expect(screen.getByTestId('Home')).toMatchSnapshot();
  });
});
