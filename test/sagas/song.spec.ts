import { expectSaga } from 'redux-saga-test-plan';

import { ActionTypes } from 'literals';
import songReducer from 'reducers/song';
import song, { getSongs } from 'sagas/song';

import { SongResponse } from 'types';

import { mergeState } from 'test-utils';

const songResponse: SongResponse = require('test/__fixtures__/song-response.json');

describe('song', () => {
  it('should have the expected watchers', async () => {
    await expectSaga(song)
      .run({ silenceTimeout: true })
      .then(result => {
        expect(result.toJSON()).toMatchSnapshot();
      });
  });

  describe('getSongs', () => {
    const params = {
      limit: 100,
      media: 'music',
      term: 'wake+me+up',
    };
    const initialAction = {
      type: ActionTypes.GET_SONGS_REQUEST,
      payload: {
        params,
      },
    };
    const initialState = () => ({
      song: songReducer.song(undefined, initialAction),
    });

    it('should handle SUCCESS', async () => {
      fetchMock.mockResponse(
        JSON.stringify({ results: songResponse.results, resultCount: songResponse.resultCount }),
      );
      return expectSaga(getSongs, {
        type: ActionTypes.GET_SONGS_REQUEST,
        payload: { params },
      })
        .withReducer(
          mergeState({
            song: {
              ...initialState(),
              songs: { data: songResponse.results },
              count: songResponse.resultCount,
            },
          }),
        )
        .run()
        .then(result => {
          expect(result.toJSON()).toMatchSnapshot();
        });
    });
  });
});
