import React from 'react';

import EmptyView from 'components/Common/EmptyView';

import { render } from 'test-utils';

describe('EmptyView', () => {
  it('should render properly', () => {
    const { container } = render(
      <EmptyView title="No result found" message="We did not find any songs for you." />,
    );
    expect(container).toMatchSnapshot();
  });
});
