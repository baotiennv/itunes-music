import React from 'react';

import Heading from 'components/Common/Heading';

import { render } from 'test-utils';

describe('Heading', () => {
  it('should render properly', () => {
    const { container } = render(<Heading>Filter Genre</Heading>);
    expect(container).toMatchSnapshot();
  });
});
