import React from 'react';

import Header from 'components/Common/Header';

import { render } from 'test-utils';

describe('Header', () => {
  it('should render properly', () => {
    const { container } = render(<Header>Header Text</Header>);
    expect(container).toMatchSnapshot();
  });
});
