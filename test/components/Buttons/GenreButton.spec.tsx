import React from 'react';
import testIds from 'constants/testIds';

import GenreButton from 'components/Buttons/GenreButton';

import { fireEvent, render, screen } from 'test-utils';

describe('Genre Button', () => {
  it('should render properly', () => {
    const handleClick = jest.fn();
    const { container } = render(
      <GenreButton
        data-testid={testIds.GENRE_BUTTON.replace('{{index}}', '0')}
        filtered={true}
        onClick={handleClick}
      >
        Blues
      </GenreButton>,
    );
    fireEvent.click(screen.getByTestId(testIds.GENRE_BUTTON.replace('{{index}}', '0')));
    expect(container).toMatchSnapshot();
    expect(handleClick).toHaveBeenCalled();
  });
});
