import React from 'react';
import testIds from 'constants/testIds';

import { SearchIcon } from 'routes/Home';

import Icon from 'components/Icon';
import SearchInput from 'components/Inputs/SearchInput';

import { fireEvent, render, screen } from 'test-utils';

describe('Search Input', () => {
  it('should search input work correctly', () => {
    const handleChange = jest.fn();
    const { container } = render(
      <SearchInput
        data-testid={testIds.SEARCH_BAR}
        size="large"
        defaultValue=""
        placeholder="Search your entertainment"
        prefix={
          <SearchIcon>
            <Icon name="search" />
          </SearchIcon>
        }
        onChange={handleChange}
      />,
    );
    fireEvent.change(screen.getByTestId('search-bar'), { target: { value: 'wake me up' } });
    expect(container).toMatchSnapshot();
    expect(handleChange).toHaveBeenCalled();
  });
});
