import { convertObjectToURLParam } from 'utils/StringUtils';

describe('String Utils', () => {
  it('should convertObjectToURLParam work correctly', () => {
    const params = {
      limit: 100,
      media: 'music',
      term: 'wake me up',
      genreId: undefined,
    };

    const url = convertObjectToURLParam(params);
    expect(url).toEqual('limit=100&media=music&term=wake%20me%20up');
  });
});
