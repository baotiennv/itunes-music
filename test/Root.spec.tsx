import React from 'react';
import App from 'Root';

import { render, screen } from 'test-utils';

const mockDispatch = jest.fn();

describe('Root', () => {
  afterEach(() => {
    mockDispatch.mockClear();
  });

  it('should render Root app', () => {
    render(<App />, { mockDispatch });
    expect(screen.getByTestId('app')).toMatchSnapshot();
  });
});
