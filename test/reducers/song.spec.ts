import { getSongs } from 'actions';
import { ActionTypes } from 'literals';
import reducer from 'reducers/song';

import { SongResponse } from 'types';

import { emptyAction } from 'test-utils';

const songResponse: SongResponse = require('test/__fixtures__/song-response.json');

describe('Song', () => {
  const params = {
    limit: 100,
    media: 'music',
    term: 'wake me up',
  };

  it('should return the initial state', () => {
    expect(reducer.song(undefined, emptyAction)).toMatchSnapshot();
  });

  describe('GET_SONGS', () => {
    it('should handle REQUEST', () => {
      expect(reducer.song(undefined, getSongs(params))).toMatchSnapshot();
    });

    it('should handle SUCCESS', () => {
      const initialState = reducer.song(undefined, getSongs(params));

      fetchMock.mockResponse(
        JSON.stringify({ results: songResponse.results, resultCount: songResponse.resultCount }),
      );

      expect(
        reducer.song(initialState, {
          type: ActionTypes.GET_SONGS_SUCCESS,
          payload: {
            songs: songResponse.results,
            params,
            count: songResponse.resultCount,
          },
        }),
      ).toMatchSnapshot();
    });

    it('should handle FAILURE', () => {
      const initialState = reducer.song(undefined, getSongs(params));
      expect(
        reducer.song(initialState, {
          type: ActionTypes.GET_SONGS_FAILURE,
          payload: { error: new Error('Something went wrong'), params },
        }),
      ).toMatchSnapshot();
    });
  });
});
