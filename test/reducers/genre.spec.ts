import reducer from 'reducers/genre';

import { emptyAction } from 'test/__setup__/test-utils';

describe('Genre', () => {
  it('should return the inital state', () => {
    expect(reducer.genre(undefined, emptyAction)).toMatchSnapshot();
  });
});
