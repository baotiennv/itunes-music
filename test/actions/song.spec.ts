import { getSongs } from 'actions/song';

describe('actions/song', () => {
  it('getSongs', () => {
    const params = {
      limit: 100,
      media: 'music',
      term: 'wake+me+up',
    };
    expect(getSongs(params)).toMatchSnapshot();
  });
});
