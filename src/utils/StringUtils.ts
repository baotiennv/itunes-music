import { PlainObject } from 'types';

export const convertObjectToURLParam = (obj: PlainObject) => {
  const str = [];
  for (const p in obj) {
    if (notEmptyString(obj[p])) {
      str.push(`${encodeURIComponent(p)}=${encodeURIComponent(obj[p])}`);
    }
  }
  return str.join('&');
};

export const notEmptyString = (str: string) => str && str !== '' && str !== null;
