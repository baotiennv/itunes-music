/**
 * Configuration
 * @module config
 */

const config = {
  name: 'Itunes Music',
  description: 'The search music engine using Apple Itunes API',
};

export default config;
