import { createAction } from 'modules/helpers';

import { ActionTypes } from 'literals';

import { PlainObject } from 'types';

export const getSongs = createAction(ActionTypes.GET_SONGS_REQUEST, (params: PlainObject) => ({
  params,
}));
