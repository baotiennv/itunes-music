import { all, call, put, takeLatest } from 'redux-saga/effects';
import { convertObjectToURLParam } from 'utils/StringUtils';

import { request } from 'modules/client';
import { getEndpoint } from 'modules/helpers';

import { ActionTypes } from 'literals';

import { PlainObject, StoreAction } from 'types';

export function* getSongs({ payload }: StoreAction) {
  const { params } = payload;
  const endpoint = `${getEndpoint()}search?${convertObjectToURLParam(params)}`;
  try {
    const response: PlainObject = yield call(request, endpoint);
    yield put({
      type: ActionTypes.GET_SONGS_SUCCESS,
      payload: { songs: response.results, params, count: response.resultCount },
    });
  } catch (error) {
    yield put({
      type: ActionTypes.GET_SONGS_FAILURE,
      payload: { error, params },
    });
  }
}

export default function* root() {
  yield all([takeLatest(ActionTypes.GET_SONGS_REQUEST, getSongs)]);
}
