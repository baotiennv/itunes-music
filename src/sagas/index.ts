import { all, fork } from 'redux-saga/effects';

import song from './song';

/**
 * rootSaga
 */
export default function* root() {
  yield all([fork(song)]);
}
