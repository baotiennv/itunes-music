import React from 'react';
import { useDispatch } from 'react-redux';
import { useDebounce } from 'react-use';
import { Col, Image, Row } from 'antd';
import testIds from 'constants/testIds';
import styled from 'styled-components';
import { notEmptyString } from 'utils/StringUtils';

import { useShallowEqualSelector } from 'modules/hooks';

import { getSongs } from 'actions';

import GenreButton from 'components/Buttons/GenreButton';
import Background from 'components/Common/Background';
import EmptyView from 'components/Common/EmptyView';
import Header from 'components/Common/Header';
import Heading from 'components/Common/Heading';
import Icon from 'components/Icon';
import SearchInput from 'components/Inputs/SearchInput';
import Loader from 'components/Loader';

import { GenreParams, GenreState, PlainObject, Song } from 'types';

export const SearchIcon = styled.div`
  margin-right: 4px;
  margin-top: 4px;
`;

const Body = styled.div`
  padding: 0px 16px;
`;

const LoadingSong = styled(Loader)`
  margin: auto;
  display: flex;
  justify-content: 'center';
`;

const PosterImage = styled.img`
  border-radius: 4px;
  object-fit: cover;
`;

const CardPoster = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0px 8px 16px 8px;
  cursor: pointer;
`;

const MusicTitle = styled.span`
  font-weight: 600;
  font-size: 16px;
  color: #0f1e36;
  margin-top: 8px;
`;

const ArtistName = styled.span`
  font-weight: 400;
  font-size: 14px;
  color: #0f1e36;
  margin-top: 4px;
  opacity: 0.5;
`;

const SearchTermWrapper = styled.div`
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const SearchTermText = styled.h3`
  font-weight: 700;
  padding: 0px 8px;
  margin-top: 24px;
`;

interface ListOfSongProps {
  isLoading?: boolean;
  songs?: Song[];
}

function ListOfSong({ isLoading, songs }: ListOfSongProps) {
  if (isLoading) {
    return <LoadingSong size={100} />;
  }
  if (songs && songs.length < 1) {
    return <EmptyView title="No result found" message="We did not find any songs for you." />;
  }
  return (
    <Row gutter={[12, 12]}>
      {songs?.map((item: Song, index: number) => (
        <Col xs={12} sm={8} md={6} lg={4} xl={3} key={index.toString()}>
          <CardPoster
            onClick={() => window.open(item?.trackViewUrl)}
            data-testid={testIds.MUSIC_POSTER.replace('{{index}}', index.toString())}
          >
            <PosterImage src={item.artworkUrl100} />
            <MusicTitle>{item.trackName}</MusicTitle>
            <ArtistName>{item.artistName}</ArtistName>
          </CardPoster>
        </Col>
      ))}
    </Row>
  );
}

export default function Home() {
  const dispatch = useDispatch();
  const genre = useShallowEqualSelector(({ genre: genreStore }) => genreStore);
  const song = useShallowEqualSelector(({ song: songStore }) => songStore);

  const initialRender = React.useRef(true);

  const [keyword, setKeyword] = React.useState(song?.params?.term);
  const [params, setParams] = React.useState<GenreParams>(
    song?.params
      ? song.params
      : {
          limit: 100,
          media: 'music',
          term: keyword,
        },
  );

  const [filtered, setFiltered] = React.useState(
    song?.params?.genreId
      ? genre.findIndex((item: GenreState) => item.genreId === song?.params?.genreId)
      : -1,
  );

  const fetchSongs = React.useCallback(
    (fetchParams: PlainObject) => {
      if (notEmptyString(keyword)) {
        dispatch(getSongs(fetchParams));
      }
    },
    [dispatch, keyword],
  );

  const handleFilterClick = React.useCallback(
    (item: GenreState, index: number) => {
      setFiltered(index === filtered ? -1 : index);
      let tempParams = params;

      if (index === filtered) {
        setFiltered(-1);
        tempParams = { ...params, genreId: undefined };
      } else {
        setFiltered(index);
        tempParams = { ...params, genreId: item.genreId };
      }

      setParams(tempParams);
      fetchSongs(tempParams);
    },
    [fetchSongs, filtered, params],
  );

  const [,] = useDebounce(
    () => {
      if (initialRender.current) {
        initialRender.current = false;
      } else {
        const tempParams = { ...params, term: keyword?.trim() };
        setParams(tempParams);
        fetchSongs(tempParams);
      }
    },
    600,
    [keyword],
  );

  return (
    <Background key="Home" data-testid="Home">
      <Header>
        <SearchInput
          data-testid={testIds.SEARCH_BAR}
          value={keyword}
          size="large"
          defaultValue=""
          placeholder="Search your entertainment"
          prefix={
            <SearchIcon>
              <Icon name="search" />
            </SearchIcon>
          }
          onChange={event => setKeyword(event.target.value)}
        />
      </Header>
      <Body>
        <Heading>Filter Genre</Heading>
        {genre.map((item: GenreState, index: number) => (
          <GenreButton
            key={index.toString()}
            onClick={() => handleFilterClick(item, index)}
            filtered={filtered === index}
            data-testid={testIds.GENRE_BUTTON.replace('{{index}}', index.toString())}
          >
            {item.label}
          </GenreButton>
        ))}
        <Heading>{`Result (${params?.term ? song?.count : '0'})`}</Heading>
        {!params.term ? (
          <SearchTermWrapper>
            <Image
              src={`${process.env.PUBLIC_URL}/media/images/magnifying-glass.png`}
              preview={false}
              width={200}
            />
            <SearchTermText>Please enter the song name you want to search</SearchTermText>
          </SearchTermWrapper>
        ) : (
          <ListOfSong isLoading={song?.songs.isLoading} songs={song?.songs?.data} />
        )}
      </Body>
    </Background>
  );
}
