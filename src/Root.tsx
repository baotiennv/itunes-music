import React from 'react';
import { Helmet } from 'react-helmet-async';
import { Route, Router, Switch } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';

import history from 'modules/history';
import theme from 'modules/theme';

import config from 'config';

import Home from 'routes/Home';
import NotFound from 'routes/NotFound';

import SystemAlerts from 'containers/SystemAlerts';

const AppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  opacity: 1 !important;
  position: relative;
  transition: opacity 0.5s;
`;

const Main = styled.main`
  min-height: 100vh;
`;

function Root() {
  return (
    <Router history={history}>
      <ThemeProvider theme={theme}>
        <AppWrapper data-testid="app">
          <Helmet
            defer={false}
            encodeSpecialCharacters={true}
            defaultTitle={config.name}
            titleTemplate={`%s | ${config.name}`}
          >
            <link
              href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,400;1,700&display=swap"
              rel="stylesheet"
            />
          </Helmet>
          <Main>
            <Switch>
              <Route component={Home} path="/" exact />
              <Route component={NotFound} />
            </Switch>
          </Main>
          <SystemAlerts />
        </AppWrapper>
      </ThemeProvider>
    </Router>
  );
}

export default Root;
