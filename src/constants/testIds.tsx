export default {
  SEARCH_BAR: 'search-bar',
  GENRE_BUTTON: 'genre-button-{{index}}',
  MUSIC_POSTER: 'music-poster-{{index}}',
};
