import { parseError } from 'modules/client';
import { createReducer } from 'modules/helpers';

import { ActionTypes } from 'literals';

import { SongState } from 'types';

export const songState: SongState = {
  songs: {
    isLoading: false,
    isError: false,
    error: '',
    data: [],
  },
  count: 0,
  params: {},
};

export default {
  song: createReducer<SongState>(
    {
      [ActionTypes.GET_SONGS_REQUEST]: (draft, { payload }) => {
        const { params } = payload;
        draft.params = params;
        draft.count = 0;
        draft.songs.isLoading = true;
        draft.songs.isError = false;
        draft.songs.error = '';
      },
      [ActionTypes.GET_SONGS_SUCCESS]: (draft, { payload }) => {
        const { params, songs, count } = payload;
        draft.params = params;
        draft.count = count;
        draft.songs.isLoading = false;
        draft.songs.isError = false;
        draft.songs.error = '';
        draft.songs.data = songs;
      },
      [ActionTypes.GET_SONGS_FAILURE]: (draft, { payload }) => {
        const { params, error } = payload;
        draft.params = params;
        draft.songs.isLoading = false;
        draft.songs.isError = true;
        draft.songs.error = parseError(error);
      },
    },
    songState,
  ),
};
