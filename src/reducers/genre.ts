import { createReducer } from 'modules/helpers';

import { GenreState } from 'types';

export const genreState: GenreState[] = [
  { label: 'Blues', genreId: 2 },
  { label: 'Classical', genreId: 5 },
  { label: 'Country', genreId: 6 },
  { label: 'Dance', genreId: 17 },
  { label: 'Electronic', genreId: 7 },
  { label: 'Hip-Hop', genreId: 18 },
  { label: 'Jazz', genreId: 11 },
  { label: 'Latin', genreId: 12 },
  { label: 'Metal', genreId: 21 },
  { label: 'R&B / Soul', genreId: 15 },
  { label: 'Reggae / Dancehall', genreId: 24 },
  { label: 'Soundtracks', genreId: 16 },
  { label: 'World', genreId: 19 },
];

export default {
  genre: createReducer({}, genreState),
};
