import app, { appState } from './app';
import genre, { genreState } from './genre';
import song, { songState } from './song';

export const initialState = {
  app: appState,
  genre: genreState,
  song: songState,
};

export default {
  ...app,
  ...genre,
  ...song,
};
