/* eslint-disable import/no-cycle */
import { PlainObject } from './common';
import { FetchingState } from './state';

export interface Song {
  wrapperType?: string;
  kind?: string;
  artistId?: number;
  collectionId?: number;
  trackId?: number;
  artistName?: string;
  collectionName?: string;
  trackName?: string;
  collectionCensoredName?: string;
  trackCensoredName?: string;
  artistViewUrl?: string;
  collectionViewUrl?: string;
  previewUrl?: string;
  artworkUrl30?: string;
  artworkUrl60?: string;
  artworkUrl100?: string;
  collectionPrice?: number;
  trackPrice?: number;
  releaseDate?: string;
  collectionExplicitness?: string;
  trackExplicitness?: string;
  discCount?: number;
  discNumber?: number;
  trackCount?: number;
  trackNumber?: number;
  trackTimeMillis?: number;
  country?: string;
  currency?: string;
  primaryGenreName?: string;
  trackViewUrl?: string;
  isStreamable?: boolean;
}

export interface SongState {
  songs: FetchingState<Song[]>;
  count: number;
  params: PlainObject;
}

export interface SongResponse {
  results: Song[];
  resultCount: number;
}
