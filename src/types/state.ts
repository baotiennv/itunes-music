/* eslint-disable import/no-cycle */
import { Dispatch } from 'redux';
import { Variants } from 'styled-minimal/lib/types';

import { AlertPosition, Icons } from './common';
import { GenreState } from './genre';
import { SongState } from './song';

export interface AlertData {
  icon: Icons;
  id: string;
  message: string;
  position: AlertPosition;
  timeout: number;
  variant: Variants;
}

export interface AppState {
  alerts: AlertData[];
}
export interface StoreState {
  app: AppState;
  genre: GenreState[];
  song: SongState;
}

export interface WithDispatch {
  dispatch: Dispatch;
}

export interface FetchingState<T> {
  isLoading?: boolean;
  isError?: boolean;
  error?: string;
  data?: T;
}
