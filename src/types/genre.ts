export type Genre =
  | 'Blues'
  | 'Classical'
  | 'Country'
  | 'Dance'
  | 'Electronic'
  | 'Hip-Hop'
  | 'Jazz'
  | 'Latin'
  | 'Metal'
  | 'R&B / Soul'
  | 'Reggae / Dancehall'
  | 'Soundtracks'
  | 'World';

export interface GenreButtonProps {
  filtered: boolean;
}

export interface GenreState {
  label: Genre;
  genreId: number;
}

export interface GenreParams {
  limit?: number;
  media?: string;
  term?: string;
  genreId?: number;
}
