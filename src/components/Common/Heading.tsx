import styled from 'styled-components';

const Heading = styled.h2`
  font-weight: 700;
  padding: 0px 8px;
  margin-top: 24px;
`;

export default Heading;
