import React from 'react';
import { Image } from 'antd';
import styled from 'styled-components';

interface EmptyViewProps {
  title: string;
  message: string;
}

const Wrapper = styled.div`
  margin: auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Title = styled.h3`
  font-weight: 700;
  padding: 0px 8px;
`;

const Message = styled.h4`
  font-weight: 400;
  padding: 0px 8px;
`;

function EmptyView({ title, message }: EmptyViewProps) {
  return (
    <Wrapper>
      <Image
        src={`${process.env.PUBLIC_URL}/media/images/empty_result.png`}
        preview={false}
        width={400}
      />
      <Title>{title}</Title>
      <Message>{message}</Message>
    </Wrapper>
  );
}

export default EmptyView;
