import styled from 'styled-components';

const Header = styled.div`
  height: 65px;
  display: flex;
  justify-content: center;
  border-bottom: 1px solid rgba(0, 0, 0, 0.1);
`;

export default Header;
