import styled from 'styled-components';

import { GenreButtonProps } from 'types';

const GenreButton = styled.div<GenreButtonProps>`
  border: ${props => (props.filtered ? '2px solid #2F904F' : '2px solid #dadada')};
  background: ${props => (props.filtered ? '#2F904F' : 'transparent')};
  color: ${props => (props.filtered ? '#fff' : ' #0f1e36')};
  font-weight: ${props => (props.filtered ? 600 : 400)};
  box-sizing: border-box;
  border-radius: 6px;
  display: inline-block;
  padding: 8px 16px;
  margin: 0px 8px 16px 8px;
  cursor: pointer;
`;

export default GenreButton;
