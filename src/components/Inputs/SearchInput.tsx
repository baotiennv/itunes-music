import { Input } from 'antd';
import styled from 'styled-components';

const SearchInput = styled(Input)`
  border: 0px;
  padding: 0px 24px;
`;

export default SearchInput;
