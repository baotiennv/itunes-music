import { keyMirror } from 'modules/helpers';

import { Status } from 'types';

export const ActionTypes = keyMirror({
  HIDE_ALERT: undefined,
  SHOW_ALERT: undefined,
  GET_SONGS_REQUEST: undefined,
  GET_SONGS_SUCCESS: undefined,
  GET_SONGS_FAILURE: undefined,
});

export const STATUS: Status = {
  IDLE: 'idle',
  RUNNING: 'running',
  READY: 'ready',
  SUCCESS: 'success',
  ERROR: 'error',
};
